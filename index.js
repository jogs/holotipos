#! /usr/bin/env node

'use strict'

/*
** Module Dependencies
*/
const program = require('commander')
const figlet = require('figlet')
const chalk = require('chalk')
const clear = require('clear')
const fs = require('fs')
const isValid = require('is-valid-path')
const validator = require('json-file-validator').full
const Holotipo = require('./lib/Holotipo')
const Type = require('./lib/Type')

/*
** Colors
*/
const error = (msg) => console.error(chalk.bold.red(msg))
const detalles = (msg) => console.error(chalk.yellow(msg))
const clave = chalk.italic.yellow
const valor = chalk.bold.blue
const clase = (arr) => chalk.bold.red(`{ ${arr.join(', ')} }`)
const titulo = (msg) => console.log(chalk.bold.italic.magenta(`\n • ${msg}:\n`))
const claveValor = (c, v) => console.log(`${clave(c)} => ${valor(v)}`)
const claveClase = (i, cl) => console.log(
  `${clave(`Clase ${i}`)} => ${clase(cl)}`)
const imprimirClase = (clase, i) => claveClase((i + 1), clase)
const claveConjunto = (cl, sets) => {
  let s = sets.map((set) => `{ ${set.join(', ')} }`)
  console.log(`${clave(cl)} => ${clase(s)}`)
}

/*
** CLI options
*/
program
.version('0.0.3')
.usage('[options] <file>')
.option('-m, --matriz', 'Imprimir matriz de semejanza')
.option('-g, --grupos', 'Imprimir pares de objetos por valor')
.option('-v, --valores', 'Imprimir conjunto de valores de la matriz')
.option('-n, --conteo', 'Imprimir conjunto de valores de la matriz (con conteo)')
.option('-u, --umbrales', 'Imprime los umbrales de semejanza')
.option('-c, --compacta <b0>', 'Clasifica utilizando la componente compacta')
.option('-r, --conexa <b0>', 'Clasifica utilizando la componente conexa',
  parseFloat)
.parse(process.argv)

// limpiar pantalla
clear()
// imprime holotipos en texto gigante
figlet('Holotipos', (err, data) => {
  if (err) {
    error('07 Algo salio mal :( ...')
    error(err)
    return
  }
  console.log(data)
  // empieza el programa
  if (program.args.length > 0) {
    let ruta = program.args[program.args.length - 1]
    if (isValid(ruta)) {
      fs.readFile(ruta, 'utf8', (err, content) => {
        if (err) error(`06 ${err}`)
        else {
          if (validator.test(content)) {
            let data = JSON.parse(content)
            // console.log(data)
            if (data.hasOwnProperty('config') && data.hasOwnProperty('lista')) {
              if (Type.isObject(data.config)) {
                if (Type.isArray(data.lista)) {
                  // todo salio bien
                  const holotipo = new Holotipo(data.config, data.lista)

                  if (program.matriz) {
                    titulo('Matriz de Semejanza')
                    Object.keys(holotipo.matriz).forEach((key) => {
                      let peer = key.split(',')
                      claveValor(`(Ob${peer[0]} - Ob${peer[1]})`,
                        holotipo.matriz[key])
                    })
                  }
                  if (program.grupos) {
                    titulo('Grupos, pares de objetos ordenados por semejanza')
                    let groups = holotipo.grupos
                    for (let key in groups) {
                      if (groups.hasOwnProperty(key)) {
                        claveConjunto(key, groups[key])
                      }
                    }
                  }
                  if (program.valores) {
                    titulo('Conjunto de valores en la matriz')
                    holotipo.valores.sort().forEach((v) => {
                      console.log(valor(v))
                    })
                  }
                  if (program.conteo) {
                    titulo('Conjunto de valores de la matriz, con conteo')
                    for (let key in holotipo.conteo) {
                      if (holotipo.conteo.hasOwnProperty(key)) {
                        claveValor(key, holotipo.conteo[key])
                      }
                    }
                  }
                  if (program.umbrales) {
                    titulo('Umbrales de Semejanza')
                    let u = holotipo.umbrales
                    claveValor('β0 Promedio', u.avg)
                    claveValor('β0 Maximo', u.max)
                    claveValor('β0 Minimo:', u.min)
                  }
                  if (program.compacta) {
                    titulo('Componente compacta')
                    if ((program.compacta > holotipo.minValue) && (
                        program.compacta <= holotipo.maxValue)) {
                      console.log(chalk.italic.dim(
                        `(se utilizo ${program.compacta} como umbral)`))
                      holotipo.componenteCompacta().map(imprimirClase)
                    } else {
                      console.log(chalk.italic.dim(
                        `(se utilizo ${program.compacta} como umbral, ${
                          chalk.underline('pero no es valido')})`))
                      holotipo.componenteConexa(1.01).map(imprimirClase)
                    }
                  }
                  if (program.conexa) {
                    program.conexa = (
                      (program.conexa > 0) && (program.conexa <= 1)
                    ) ? program.conexa : 0
                    titulo('Componente conexa')
                    console.log(chalk.italic.dim(
                      `(se utilizo ${program.conexa} como umbral)`))
                    holotipo.componenteConexa(program.conexa).map(imprimirClase)
                  }
                  // console.log('conexa', program.conexa)
                } else {
                  error('05 El Atributo "lista" debe ser un arreglo de objetos')
                }
              } else {
                error('04 El Atributo "config" debe ser un objeto')
              }
            } else {
              error('03 El arhivo con contiene suficiente información')
              detalles('El archivo requiere los atributos "config" y "lista"')
            }
          } else {
            error('02 El archivo no tiene un formato JSON estandar')
          }
        }
      })
    } else {
      error('01 La ruta del archivo no es valida')
    }
  } else {
    error('00 No Especifico la direccion del archivo')
  }
})
