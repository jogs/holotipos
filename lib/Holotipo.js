'use strict'
const Comparador = require('./Comparador')
const Network = require('./Network')
const Type = require('./Type')
const _ = require('underscore')

module.exports = class Holotipo {
  constructor (criterio, objects) {
    if (Type.isArray(objects)) {
      // numero de objetos
      this.size = objects.length
      // matriz de semejanza
      this.sm = []
      // matriz imprimible
      this.matrix = {}
      let c = new Comparador(criterio)
      for (let i = 0; i < objects.length - 1; i++) {
        let current = []
        for (let j = (i + 1); j < objects.length; j++) {
          let r = c.comparacion(objects[i], objects[j]).average
          this.matrix[`${i},${j}`] = r
          current = [r, ...current]
        }
        this.sm = [current, ...this.sm]
      }
      let preSet = []
      for (let key in this.matrix) {
        preSet = [...preSet, this.matrix[key]]
      }
      // pares de objetos agrupados por valor
      this.group = {}
      new Set(preSet.sort()).forEach((value) => {
        this.group[value] = []
        let current = new Set([])
        for (let key in this.matrix) {
          if (this.matrix[key] === value) {
            current.add(key)
          }
        }
        this.group[value] = [...current].sort().map((s) => s.split(',')
        .map((n) => Number(n)))
      })
      // contador de valores
      this.count = {}
      for (let key in this.group) {
        if (this.group.hasOwnProperty(key)) {
          this.count[key] = this.group[key].length
        }
      }
    }
  }
  // elimina elementos vacios
  _clean (arr) {
    return arr.filter((e) => (e.length > 0))
  }
  // separa un arreglo en sub arreglos
  _separarArreglo (arr) {
    return arr.map((e) => [e])
  }
  _clasificarPor (umbral, menor = false) {
    // el umbral se hace numerico
    if (!Type.isNumber(umbral)) umbral = Number(umbral)
    // se obtene el conjunto de valores de la matriz
    let values = this.valores
    // se filtan solo los valores que cumplen el umbral
    if (menor) {
      values = values.filter((v) => v <= umbral)
    } else {
      values = values.filter((v) => v >= umbral)
    }
    // Se genera el grafo con los vertices (uno por cada objeto)
    let graph = new Network()
    for (let i = 0; i < this.size; i++) {
      graph.addVertex(i)
    }
    // si el umbral es mayor que el valor maximo de la matriz son counjuntos
    // aislados, (cada objeto queda solo en una clase)
    if (umbral > this.maxValue) {
      return this._separarArreglo(graph.vertices)
    }
    // el objeto group tiene como llave el miembros del conjunto de valores de
    // la matriz y el valore para cada llave es un arreglo de coordenadas,
    // que corresponden a las comparaciones, con estos pares se forman los edges
    values.forEach((value) => {
      (this.group[String(value)]).forEach((peer) => {
        graph.addEdge(...peer)
      })
    })
    // se excluyen los objetos que no estan relacionados con otros
    // (solo los objetos con edges)
    let relationed = graph.relationed.sort()
    // objetos sin edges
    let unrelationed = _.difference(graph.vertices, relationed)
    // se obtiene el objeto con mas edges y se hace un expand (se obtienen sus
    // vecinos y los vecinos de los vecinos)
    let popular = _.union(...graph.expand(graph.mostPopularVertex(), 3)).sort()
    // si popular y relacionados es igual significa que todos forman una sola
    // clase, por lo que se envia relacinados y no relacionados como respuesta
    if (_.isEqual(relationed, popular)) {
      // todas las relaciones son compartidas
      return (unrelationed.length > 0) ? this._clean([relationed, ...this._separarArreglo(unrelationed)]) : [relationed]
    } else {
      // si el no se cumple el caso anterior se saca la diferencia entre
      // relacionados y pospular, esto sinifica que son los que no pertenecen
      // al grupo mas grande
      let diff = _.difference(relationed, popular).sort()
      // si la diferencia es igual a los relacionados, significa que solo
      // existe una relacion
      if (_.isEqual(diff, relationed)) {
        // solo existe una relacion
        return (unrelationed.length > 0) ? this._clean([relationed, ...this._separarArreglo(unrelationed)]) : [relationed]
      } else {
        // console.log('diff', diff)
        // console.log('relationed', relationed)
        // se guardan la relacion mas grande y los no relacionados
        let result = (unrelationed.length > 0) ? [
          _.difference(relationed, diff).sort(), ...this._separarArreglo(unrelationed)] : [relationed]
        // console.log('result', result)
        // mientras exista diferencia (aun no se han ubicado todas las relaciones)
        while (diff.length > 0) {
          console.log('=================================')
          // se obtiene los vecinos del primer elemento de los sobrantes
          let group = _.union(...graph.expand(diff[0], 3)).sort()
          // console.log('g', group)
          // console.log('d', diff)
          // se actualiza el nuevo grupo de los sobrantes
          diff = _.difference(diff, group).sort()
          // console.log('nDiff', diff)
          // console.log('oR', result)
          // result = result.map((cls) => _.difference(cls, group))
          // el grupo formado se añade al resultado
          result = [group, ...result]
          // console.log('nR', result)
        }
        // se limipia y ordena el resultado (se le quitan las clases vacias)
        return this._clean(result).sort()
      }
    }
  }
  get umbrales () {
    let acum = 0
    for (let key in this.count) {
      if (this.count.hasOwnProperty(key)) {
        acum += this.count[key] * Number(key)
      }
    }
    let avg = ((2 * acum) / (this.size * this.size - 1)).toFixed(2)
    let max = 0
    for (let i = 0; i < this.sm.length; i++) {
      max += _.max(this.sm[i])
    }
    max = (max / this.sm.length).toFixed(2)
    return {avg, min: this.minValue, max}
  }
  get valores () {
    return Object.keys(this.count).map((k) => Number(k))
  }
  get grupos () {
    return this.group
  }
  get conteo () {
    return this.count
  }
  get maxValue () {
    return Math.max(...this.valores)
  }
  get minValue () {
    return Math.max(...this.valores)
  }
  get matriz () {
    return this.matrix
  }
  componenteCompacta () {
    return this._clasificarPor(this.maxValue)
  }
  componenteConexa (b0) {
    return this._clasificarPor(b0)
  }
}
