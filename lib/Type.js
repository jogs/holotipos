const number = /\-?(?:0|[1-9][\d]*)(?:\.[\d]+)?(?:[eE][\+\-]?[\d]*)?/
const interval = new RegExp(`${/^\s*(\[|\()\s*/.source}(${number.source
})${/\s*\,\s*/.source}(${number.source})${/\s*(\]|\))\s*$/.source}`)
module.exports = class ValidatorType {
  constructor () {
    //
  }
  static isNumber (data) {
    return (typeof data === 'number')
  }
  static isString (data) {
    return (typeof data === 'string')
  }
  static isNull (data) {
    return (data === null)
  }
  static isEmpty (data) {
    return (data === null || data === [] || data === {})
  }
  static isBoolean (data) {
    return (typeof data === 'boolean')
  }
  static isArray (data) {
    return Array.isArray(data)
  }
  static isObject (data) {
    return (typeof data === 'object' && !Array.isArray(data) && data !== null)
  }
  static isInterval (data) {
    // console.log(this.interval)
    return (interval.test(String(data)))
  }
  static parseInterval (data) {
    let parsed = data.match(interval)
    if (parsed.length === 5) {
      return [parsed[1], parsed[2], parsed[3], parsed[4]]
    } else return null
  }
}
