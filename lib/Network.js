module.exports = class Graph {
  constructor () {
    this.vertices = []
    this.edges = []
    this.numberOfEdges = 0
  }
  addVertex (v) {
    this.vertices = [...this.vertices, v]
    this.edges[v] = []
  }
  addEdge (v1, v2) {
    this.edges[v1] = [...this.edges[v1], v2]
    this.edges[v2] = [...this.edges[v2], v1]
    this.numberOfEdges++
  }
  absoluteRelationOf (n) {
    if (n < this.vertices.length) {
      return [n, ...this.edges[n]].sort()
    } else return null
  }
  neighborsOf (n) {
    if (n < this.vertices.length) {
      return this.edges[n]
    }
  }
  expand (n, levels = 1) {
    levels = Math.abs(levels)
    let result = [[n], this.neighborsOf(n)]
    if (levels > 1) {
      for (let i = 1; i < levels; i++) {
        let all = []
        result[i].forEach((v) => {
          all = [...all, ...this.neighborsOf(v)]
        })
        result = [...result, [...(new Set(all))]]
      }
    }
    return result
  }
  mostPopularVertex () {
    let index = 0
    this.edges.forEach((edge, i) => {
      if (this.edges[i].length < edge.length) index = i
    })
    return index
  }
  get size () {
    return this.vertices.length
  }
  get relations () {
    return this.numberOfEdges
  }
  get relationed () {
    return this.vertices.filter((v, i) => (this.edges[i].length > 0))
  }
}
