const Type = require('./Type')
const number = new Set(['igualdad', 'conjunto', 'intervalo', 'umbral'])
const string = new Set(['igualdad', 'conjunto'])
const boolean = new Set(['igualdad', 'conjunto'])
const other = new Set(['igualdad'])

module.exports = class Comparador {
  constructor (conf) {
    if (Type.isObject(conf)) {
      this.schema = conf
      this.attributes = Object.keys(conf)
      this.max = 0
      for (let attribute in this.schema) {
        if (this.schema.hasOwnProperty(attribute)) {
          this.max += this.schema[attribute].peso || 1
        }
      }
      return this
    } else return null
  }
  comparacion (o1, o2) {
    if (Type.isObject(o1) && Type.isObject(o2)) {
      let data = {}
      let average = 0
      this.attributes.map((attribute) => {
        if (o1.hasOwnProperty(attribute) && o2.hasOwnProperty(attribute)) {
          data[attribute] = 0
          if (typeof o1[attribute] === typeof o2[attribute]) {
            let match = false
            switch (typeof o1[attribute]) {
              case 'number':
                match = number.has(this.schema[attribute].criterio)
                break
              case 'string':
                match = string.has(this.schema[attribute].criterio)
                break
              case 'boolean':
                match = boolean.has(this.schema[attribute].criterio)
                break
              default:
                match = other.has(this.schema[attribute].criterio)
            }
            if (match) {
              data[attribute] = (this[this.schema[attribute].criterio](
                o1[attribute], o2[attribute], (this.schema[attribute].x || null)
              ) * (this.schema[attribute].peso || 1))
            }
          }
          average += data[attribute]
        }
      })
      average = Math.round((average / this.max) * 100) / 100
      return {data, average}
    } return null
  }
  igualdad (a, b) {
    return Number(a === b)
  }
  _entre (numero, limite) {
    let isIn = false
    if (limite[0] === '[') {
      isIn = (limite[1] <= numero)
    } else {
      isIn = (limite[1] < numero)
    }
    if (limite[3] === ']') {
      isIn = (limite[2] >= numero) && isIn
    } else {
      isIn = (limite[2] > numero) && isIn
    }
    return isIn
  }
  _intervalo (a, b, limite) {
    if (Type.isInterval(limite)) {
      let intervalo = Type.parseInterval(limite)
      return (this._entre(a, intervalo) && this._entre(b, intervalo))
    }
  }
  intervalo (a, b, limites = []) {
    if (Type.isArray(limites)) {
      let flag = false
      limites.map((limite) => {
        flag = (flag || this._intervalo(a, b, limite))
      })
      return Number(flag)
    } return 0
  }
  _conjunto (a, b, elementos = []) {
    let conjunto = new Set(elementos)
    return Number(conjunto.has(a) && conjunto.has(b))
  }
  conjunto (a, b, conjuntos = []) {
    let result = 0
    conjuntos.forEach((conjunto) => {
      if (this._conjunto(a, b, conjunto)) {
        result = 1
      }
    })
    return result
  }
  umbral (a, b, umbral) {
    return Number(Math.abs(a - b) <= umbral)
  }
}

// let criterio = new Criterio({
//   one: {
//     criterio: 'igualdad',
//     peso: 0.9
//   },
//   two: {
//     criterio: 'umbral',
//     peso: 0.8,
//     x: 25
//   },
//   three: {
//     criterio: 'intervalo',
//     peso: 0.5,
//     x: ['[0, 100)', '[200, 300)', '[300, 400]']
//   },
//   four: {
//     criterio: 'conjunto',
//     x: [['a', 'e', 'i', 'o', 'u'], ['x', 'y', 'z']]
//   }
// })

// let result = criterio.comparacion({
//   one: true,
//   two: 10,
//   three: 99,
//   four: 'a'
// }, {
//   one: true,
//   two: 36,
//   three: 98,
//   four: 'u'
// })

// console.log(result)
