Holotipos
------------

Clasificación no supervisada, utilizando el algoritmo de Holotipos

# Instalación

_Requiere la previa instalación de Node y NPM_

``` bash
npm install holotipos -g
```

# Uso

Recibe un archivo en formato [JSON](http://www.json.org/)

El archivo require dos atributos
* **config** de tipo objeto
* **lista** de tipo arreglo

_Ejemplo_
```json
{
  "config": {
    "one": {
      "criterio": "igualdad",
      "peso": 0.9
    },
    "two": {
      "criterio": "umbral",
      "peso": 0.8,
      "x": 25
    },
    "three": {
      "criterio": "intervalo",
      "peso": 0.5,
      "x": ["[0, 100)", "[200, 300)", "[300, 400]"]
    },
    "four": {
      "criterio": "conjunto",
      "x": ["a", "e", "i", "o", "u"]
    }
  },
  "lista": [
    {
      "one": true,
      "two": 10,
      "three": 99,
      "four": "a"
    }, {
      "one": false,
      "two": 36,
      "three": 101,
      "four": "e"
    }, {
      "one": false,
      "two": 35,
      "three": 100,
      "four": "i"
    }, {
      "one": true,
      "two": 20,
      "three": 200,
      "four": "b"
    }, {
      "one": true,
      "two": 50,
      "three": 299,
      "four": "p"
    }, {
      "one": false,
      "two": 70,
      "three": 100,
      "four": "i"
    }, {
      "one": false,
      "two": 80,
      "three": 300,
      "four": "b"
    }, {
      "one": false,
      "two": 10,
      "three": 400,
      "four": "ñ"
    }, {
      "one": true,
      "two": 265,
      "three": 220,
      "four": "o"
    }, {
      "one": false,
      "two": 299,
      "three": 18,
      "four": "w"
    }, {
      "one": true,
      "two": 20,
      "three": 200,
      "four": "b"
    }, {
      "one": true,
      "two": 20,
      "three": 400,
      "four": "a"
    }
  ]
}
```

Para ejecutar el programa tiene que ingresar el comando holotipos

``` txt
holotipos [opciones] <archivo>
```

## Opciones

* **-m, --matriz**       Imprimir matriz de semejanza
* **-v, --valores**      Imprimir conjunto de valores de la matriz
* **-u, --umbrales**     Imprime los umbrales de semejanza
* **-c, --compacta**     Clasifica utilizando la componente compacta
* **-r, --conexa <b0>**  Clasifica utilizando la componente conexa

# Ejemplo

Para visualizar todas las posibles Opciones

_modo descriptivo_

``` bash
holotipos --matriz --valores --umbrales --compacta --conexa 0.5 ./ejemplo.json
```

_modo rapido_

``` bash
holotipos -m -v -u -c -r 0.5 ./ejemplo.json
```
